# DataSystemsAPI
Desarrollo de API examen tecnico para DataSystems


## Getting started

Para poder correr este proyecto se debe tener la version de VisualStudio 2019 o 2022 y base de Datos SQL Server o posterior


Este proyecto sde desarrollo utilizando en C# utilizando NetCore 3.1 y EntityFrameworkCore para el mapeo de datos utilizando CodeFirst y SQL Server 2019 para El almacenamiento de datos.

Para restaurar la base de datos se pueden utilizar los scripts proporcionados o tambien se puede realizar una migracion con EntityFramework.
## Add your files




## Authors and acknowledgment
Pedro Adan Bustos

## License
MIT Licence

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
